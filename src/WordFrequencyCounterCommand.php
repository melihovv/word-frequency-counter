<?php

namespace WordFrequencyCounter;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class WordFrequencyCounterCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('count:word-frequency')
            ->setDescription('Count frequency of each word in the specified file.');

        $this->addArgument('file', InputArgument::REQUIRED, 'The path to the file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $wordFrequencies = (new WordFrequencyCounter())->count($input->getArgument('file'));

            $table = new Table($output);
            $table
                ->setHeaders(['Word', 'Frequency'])
                ->setRows(array_map(function ($word, $frequency) {
                    return [$word, $frequency];
                }, array_keys($wordFrequencies), $wordFrequencies))
                ->render();

            return 0;
        } catch (Throwable $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");

            return 1;
        }
    }
}
