<?php

namespace WordFrequencyCounter;

use InvalidArgumentException;

class WordFrequencyCounter
{
    /**
     * Count the frequency of each word in the specified file.
     *
     * @param string $filePath
     * @return array Returns an associative array of words as keys and their frequencies as value.
     * @throws InvalidArgumentException
     */
    public function count($filePath)
    {
        $handle = $this->openFile($filePath);
        $wordFrequencies = [];

        while (!feof($handle)) {
            $line = preg_replace('/[^[:alpha:] ]/u', '', mb_strtolower(trim(fgets($handle))));

            if ($line === '') {
                continue;
            }

            foreach (explode(' ', $line) as $word) {
                if ($word === '') {
                    continue;
                }

                if (isset($wordFrequencies[$word])) {
                    $wordFrequencies[$word] += 1;
                } else {
                    $wordFrequencies[$word] = 1;
                }
            }
        }

        fclose($handle);

        arsort($wordFrequencies);

        return $wordFrequencies;
    }

    /**
     * @param string $filePath
     * @return resource
     * @throws InvalidArgumentException
     */
    private function openFile($filePath)
    {
        if (!file_exists($filePath)) {
            throw new InvalidArgumentException("File does not exist: [$filePath]");
        }

        $handle = fopen($filePath, 'rb');
        if (!$handle) {
            throw new InvalidArgumentException("Cannot open file: [$filePath]");
        }

        return $handle;
    }
}
