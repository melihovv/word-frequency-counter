# Задание

На входе в программу текстовый файл input со словами, разделенными пробелами. 
Файл содержит >1 строку. Ограничения по размеру файла нет (рассмотреть работу с большими файлами) 

Сравнение без учета регистра, цифры и спец символы не учитываем, учитываем только буквы. русская и английская кодировка. 

На выходе в файле output:  
%слово_1% - %частота_вхождения%  
%слово_2% - %частота_вхождения%  
%слово_N% - %частота_вхождения%  

Сортировка в файле от наиболее частого к менее часто встречающемуся слову в input. 

input  
——  
test tost tst  
test tost  
test  
tstt  

output  
——  
test - 3  
tost - 2  
tstt - 1  
tst - 1  

style guide + unit ожидаем дефолтно

# Requirements
- git
- php>=7.0
- composer

# Install
- `git clone https://bitbucket.org/melihovv/word-frequency-counter`
- `cd word-frequency-counter`
- `composer install`

# Usage
`php index.php count:word-frequency input.txt`

# Run tests
`vendor/bin/phpunit tests`
