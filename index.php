<?php

require_once __DIR__ . '/vendor/autoload.php';

use WordFrequencyCounter\WordFrequencyCounterCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new WordFrequencyCounterCommand());

$application->run();
