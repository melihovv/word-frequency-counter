<?php

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use WordFrequencyCounter\WordFrequencyCounter;

class WordFrequencyCounterTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    public function setUp()
    {
        $this->root = vfsStream::setup('root', null, [
            'empty-example.txt' => '',
            'single-line-example.txt' => ' a b c a',
            'multi-line-example.txt' => '
                a b c
                b c
                c a a
            ',
            'digits-and-special-symbols-example.txt' => 'a b - c, c5 55',
            'different-registry-example.txt' => 'a A',
            'russian-example.txt' => 'а б в в б б б',
        ]);
    }

    /** @test */
    function it_counts_words_frequency_in_single_line_file()
    {
        $wordFrequencies = (new WordFrequencyCounter())->count($this->root->url() . '/single-line-example.txt');

        $this->assertEquals([
            'a' => 2,
            'b' => 1,
            'c' => 1,
        ], $wordFrequencies);
    }

    /** @test */
    function it_counts_words_frequency_in_milti_line_file()
    {
        $wordFrequencies = (new WordFrequencyCounter())->count($this->root->url() . '/multi-line-example.txt');

        $this->assertEquals([
            'a' => 3,
            'c' => 3,
            'b' => 2,
        ], $wordFrequencies);
    }

    /** @test */
    function it_returns_empty_array_if_specified_file_is_empty()
    {
        $wordFrequencies = (new WordFrequencyCounter())->count($this->root->url() . '/empty-example.txt');

        $this->assertEquals([], $wordFrequencies);
    }

    /** @test */
    function it_ignores_digits_and_special_symbols()
    {
        $wordFrequencies = (new WordFrequencyCounter())->count($this->root->url() . '/digits-and-special-symbols-example.txt');

        $this->assertEquals([
            'c' => 2,
            'a' => 1,
            'b' => 1,
        ], $wordFrequencies);
    }

    /** @test */
    function it_counts_russian_word_frequencies()
    {
        $wordFrequencies = (new WordFrequencyCounter())->count($this->root->url() . '/russian-example.txt');

        $this->assertEquals([
            'б' => 4,
            'в' => 2,
            'а' => 1,
        ], $wordFrequencies);
    }

    /** @test */
    function it_counts_case_insensitively()
    {
        $wordFrequencies = (new WordFrequencyCounter())->count($this->root->url() . '/different-registry-example.txt');

        $this->assertEquals([
            'a' => 2,
        ], $wordFrequencies);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    function it_throws_error_if_specified_file_does_not_exist()
    {
        (new WordFrequencyCounter())->count($this->root->url() . '/not-exist.txt');
    }
}

